package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class AddUserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_get_200_as_status_code_and_content_type_is_text_plain() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN));
    }

    @Test
    void should_left_aligned() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus")).andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        String[] arrResult = result.split("\n");

        boolean flag = true;
        for (int i = 0; i < arrResult.length; i++) {
            int ind = arrResult[i].indexOf('0');
            if (ind != -1 && i > 0 && arrResult[i-1].length() > ind) {
                if (arrResult[i-1].charAt(ind) != ' ') {
                    flag = false;
                    break;
                }
            }
        }
        assert flag;
    }
}
