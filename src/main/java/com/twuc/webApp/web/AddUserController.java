package com.twuc.webApp.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AddUserController {
    @GetMapping("/api/tables/plus")
    private String generate_add_table() {
        StringBuilder addTable = new StringBuilder("");
        for (int i = 1; i <= 9; i++){
            for (int j = 1; j <= i; j++){
                int sum = i + j;
                addTable.append(i).append('+').append(j).append('=').append(sum).append(' ');
                if(sum < 10)
                    addTable.append(' ');
            }
            addTable.append('\n');
        }
        return addTable.toString();
    }
    public String getTable() {
        return generate_add_table();
    }
}
