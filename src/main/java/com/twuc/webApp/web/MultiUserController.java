package com.twuc.webApp.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MultiUserController {
    @GetMapping("/api/tables/multiply")
    private String generate_add_table() {
        StringBuilder multiTable = new StringBuilder("");
        for (int i = 1; i <= 9; i++){
            for (int j = 1; j <= i; j++){
                int product = i * j;
                multiTable.append(i).append('*').append(j).append('=').append(product).append(' ');
                if(product < 10)
                    multiTable.append(' ');
            }
            multiTable.append('\n');
        }
        return multiTable.toString();
    }
    public String getTable() {
        return generate_add_table();
    }
}
